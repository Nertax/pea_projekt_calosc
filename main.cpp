#include"TSP/Tsp.hpp"
#include<iostream>
#include <TSP/BruteForce.hpp>
#include <TSP/BnB.hpp>
#include <TSP/HeldKarp.hpp>
#include <TSP/SimulatedAnnealing.hpp>
#include <TSP/GeneticAlgorithm.hpp>
#include"TSP/Measure.hpp"

int main(int argc, char **argv) {


    TSPresult res;

    std::cout << "Presentation of implemented algorithms" << std::endl;

    std::cout << "Brute force for test_data/tsp_10.txt:" << std::endl;
    BruteForce bf;
    bf.ReadMatrixFromFile("../test_data/tsp_10.txt");
    res = bf.RunBruteForce();
    std::cout << res.toString(0) << std::endl << std::endl;

    std::cout << "BnB for test_data/tsp_15.txt:" << std::endl;
    BnB bnb;
    bnb.ReadMatrixFromFile("../test_data/tsp_15.txt");
    res = bnb.BnBDFS(&BnB::GetBoundByAvgOfMinValueInRowAndColumn);
    std::cout << res.toString(0) << std::endl << std::endl;

    std::cout << "Held Karp for test_data/tsp_17.txt:" << std::endl;
    HeldKarp hk;
    hk.ReadMatrixFromFile("../test_data/tsp_17.txt");
    res = hk.RunHeldKarp();
    std::cout << res.toString(0) << std::endl << std::endl;

    std::cout << "Simulate annealing for test_data/data100.txt" << std::endl;
    SimulatedAnnealing sa;
    sa.ReadMatrixFromFile("../test_data/data100.txt");
    sa.SetIterationsLeft(5000);
    std::cout << "Best path weight: 36230" << std::endl;
    res = sa.RunSimulatedAnnealing(1000, 0.01, 1, 0, 500, &TSP::GetNewResultByInsert);
    std::cout << res.toString(0) << std::endl << std::endl;

    std::cout << "Genetic algorithm for test_data/data100.txt" << std::endl;
    GeneticAlgorithm ga;
    ga.ReadMatrixFromFile("../test_data/data100.txt");
    std::cout << "Best path weight: 36230" << std::endl;
    res = ga.RunGeneticAlgorithm(100, 500000000, 0.6, 0.1);
    std::cout << res.toString(0) << std::endl << std::endl;

    std::cout << "Hybrid algorithm for test_data/data100.txt" << std::endl;
    GeneticAlgorithm ha;
    ha.ReadMatrixFromFile("../test_data/data100.txt");
    std::cout << "Best path weight: 36230" << std::endl;
    res = ha.RunHybridAlgorithm(100, 500000000, 0.6, 0.1);
    std::cout << res.toString(0) << std::endl << std::endl;


    return 0;

}
