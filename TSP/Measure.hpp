#include<iostream>
#include<fstream>
#include<queue>
#include "GeneticAlgorithm.hpp"
#include "Time.hpp"


#ifndef Measure_h
#define Measure_h


class Measure {

private:
    void _AutoMeasurements();
    int _ExecuteMeasureTSP_GA(uint numberOfMeasurements, std::string fileNameOfCitiesMatrix, ulong bestKnownPathWeight,
                                   std::string fileName);

public:
    void Start();

};

#endif
