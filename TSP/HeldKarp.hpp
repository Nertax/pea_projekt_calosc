#include "Tsp.hpp"

#ifndef PROJEKT2_HELDKARP_HPP
#define PROJEKT2_HELDKARP_HPP

class HeldKarp : public TSP {

public:
    TSPresult RunHeldKarp();

};

#endif //PROJEKT2_HELDKARP_HPP
