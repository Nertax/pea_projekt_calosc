#include "SimulatedAnnealing.hpp"


TSPresult SimulatedAnnealing::_GetRandomResult() {


    TSPresult result;
    std::vector<uint16_t> tempCities;

    for (uint16_t i = 0; i < citiesAmount; i++)
        if (i != startCity)
            tempCities.push_back(i);


    std::mt19937 randomEngine(time(0));


    while (!tempCities.empty()) {
        std::uniform_int_distribution<int> distribution(0, tempCities.size() - 1);
        int number = distribution(randomEngine);
        result.shortestPath.push_back(tempCities[number]);
        std::vector<uint16_t>::iterator it = tempCities.begin();
        tempCities.erase(it + number);
    }

    result.pathWeight = std::numeric_limits<int64_t>::max();

    return result;


}


double SimulatedAnnealing::_GetCurrentProbability(int64_t currentPathWeight, int64_t newPathWeight,
                                                  double currentTemperature) {
    return 1.0 / (1.0 + pow(M_E, ((currentPathWeight - newPathWeight) / currentTemperature)));
}

double SimulatedAnnealing::_GetNewTemperatureL(double currentTemperature, double coolingRate) {

    return currentTemperature * (1.0 - coolingRate);
}


TSPresult SimulatedAnnealing::RunSimulatedAnnealing(double startTemperature, double coolingRate, double aboveTemperatureChooseNeighbour,
                                  double temperatureUp, uint iterationsAmountToRestart,
                                  TSPresult (TSP::*getNewResult)(TSPresult &oldResult,
                                                                 const std::vector<uint16_t> &pair)) {
    int iterations = 0;
    double temperature = startTemperature;
    this->_localMinAmount = 0;

    double iterationsLeftWhenLastBetterResultFound = _iterationsLeft;

    TSPresult result = _GetRandomResult();
    TSPresult bestResult = result;

    Subset::setLength(citiesAmount - 1);
    Subset::resetVectorOfSubsets();
    std::vector<std::vector<uint16_t>> setOfPairsForGenerateNeighbours = Subset::getVectorOfSubsets(2);

    std::mt19937 randomEngine(time(0));
    std::uniform_int_distribution<int> distribution(0, setOfPairsForGenerateNeighbours.size() - 1);
    auto generator = std::bind(distribution, randomEngine);

    std::default_random_engine engineForDecision;
    std::uniform_real_distribution<double> distributionForDecision(0.0, 1.0);
    auto getRandomNumberToMakeDecision = std::bind(distributionForDecision, engineForDecision);


    while (_iterationsLeft > 0) {

        _iterationsLeft--;
        iterations++;

        if (iterationsLeftWhenLastBetterResultFound - _iterationsLeft > iterationsAmountToRestart) {
            result = _GetRandomResult();
            temperature = startTemperature;
            iterationsLeftWhenLastBetterResultFound = _iterationsLeft;
        }

        int leftNeighboursAmount = setOfPairsForGenerateNeighbours.size();
        std::vector<bool> usedNeighbourFlags(setOfPairsForGenerateNeighbours.size());

        while (leftNeighboursAmount) {

            int numberOfPairToUse;

            while (true) {
                numberOfPairToUse = generator();
                if (usedNeighbourFlags[numberOfPairToUse] == false) {
                    usedNeighbourFlags[numberOfPairToUse] = true;
                    break;
                } else
                    continue;
            }

            TSPresult tempResult = (this->*getNewResult)(result, setOfPairsForGenerateNeighbours[numberOfPairToUse]);

            if (tempResult.pathWeight < result.pathWeight) {
                result.pathWeight = tempResult.pathWeight;
                result.shortestPath = tempResult.shortestPath;

                if (result.pathWeight < bestResult.pathWeight) {
                    bestResult.pathWeight = result.pathWeight;
                    bestResult.shortestPath = result.shortestPath;
                    iterationsLeftWhenLastBetterResultFound = _iterationsLeft;
                }

                break; // greedy

            } else if (getRandomNumberToMakeDecision() <=
                       1 - _GetCurrentProbability(result.pathWeight, tempResult.pathWeight,
                                                  temperature)) //without this else if - local search, with  - simulated annealing
            {
                result.pathWeight = tempResult.pathWeight;
                result.shortestPath = tempResult.shortestPath;
                break;

            } else {
                leftNeighboursAmount--;

                usedNeighbourFlags[numberOfPairToUse] = false;

                if (leftNeighboursAmount == 0) {

                    this->_localMinAmount++;

                    if (temperature > aboveTemperatureChooseNeighbour) {
                        result = (this->*getNewResult)(result, setOfPairsForGenerateNeighbours[generator()]);
                        temperature += temperatureUp;
                        break;

                    } else {
                        result = _GetRandomResult();
                        temperature = startTemperature;
                        break;
                    }
                }
            }
        }

        temperature = _GetNewTemperatureL(temperature, coolingRate);

    }

    return bestResult;
}

uint SimulatedAnnealing::GetLocalMinAmount() const {
    return _localMinAmount;
}


void SimulatedAnnealing::SetIterationsLeft(uint iterationsLeft) {

    this->_iterationsLeft = iterationsLeft;
}
