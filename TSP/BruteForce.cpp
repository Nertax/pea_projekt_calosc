#include"BruteForce.hpp"

TSPresult BruteForce::RunBruteForce() {


    TSPresult result;

    int64_t weight = 0;
    std::vector<uint16_t> path;

    for(uint16_t i = 0; i < citiesAmount; i++)
        if(i != startCity)
            path.push_back(i);


    weight += citiesMatrix[startCity][path[0]];

    for(size_t i = 0; i < path.size() - 1; i++)
        weight += citiesMatrix[path[i]][path[i+1]];

    weight += citiesMatrix[path.back()][startCity];

    result.shortestPath = path;
    result.pathWeight = weight;


    while(std::next_permutation(path.begin(), path.end())) {

        weight = 0;
        weight += citiesMatrix[startCity][path[0]];

        for(size_t i = 0; i < path.size() - 1; i++)
            weight += citiesMatrix[path[i]][path[i+1]];

        weight += citiesMatrix[path.back()][startCity];

        if(weight < result.pathWeight) {
            result.pathWeight = weight;
            result.shortestPath = path;
        }

    }


    return result;
}