#include"Measure.hpp"


void Measure::Start() {

    _AutoMeasurements();
}


void Measure::_AutoMeasurements() {

    uint numberOfMeasurements = 40;
    _ExecuteMeasureTSP_GA(numberOfMeasurements, "../test_data/data323.txt", 1326, "../results/323HA");

}

int Measure::_ExecuteMeasureTSP_GA(uint numberOfMeasurements, std::string fileNameOfCitiesMatrix, ulong bestKnownPathWeight,
                                    std::string fileName)
{

    std::vector<uint> allPopulationSize{100};
    std::vector<long> allTimeForAlgorithm{2600000000};
    std::vector<double> allCrossoverProbability{0.6};
    std::vector<double> allMutationProbability{0.1};



    GeneticAlgorithm tsp;
    tsp.ReadMatrixFromFile(fileNameOfCitiesMatrix);

    long long totalTime;
    long long totalError;
    uint totalGenerationsAmount;
    struct timespec a, b;


    double currentAboveTemperatureChooseNeighbour = 0;
    double currentTemperatureUp = 0;
    uint currentIterationsAmountToRestart = 0;

    std::fstream file;
    file.open(fileName.c_str(), std::ios::out | std::ios::app);

    if (file.is_open()) {

        file
                << "Wielkosc populacji;Zadany czas;Prawd. krzyzowania;Prawd. mutacji;Ilosc pokolen;Rzeczywisty czas; Sredni blad bezwzgledny;Sredni blad wzgledny"
                << std::endl;

        if (file.fail()) {
            std::cout << "Blad zapisu wyniku pomiaru do pliku, przerywam pomiary."
                      << std::endl;
            return -1;
        }

        for (uint currentPopulationSize : allPopulationSize) {
            for (long currentTimeForAlgorithm : allTimeForAlgorithm) {
                for (double currentCrossoverProbability : allCrossoverProbability) {
                    for (double currentMutationProbability : allMutationProbability) {


                        totalTime = 0;
                        totalError = 0;
                        totalGenerationsAmount = 0;
                        uint z = numberOfMeasurements;


                        while (z--) {

                            clock_gettime(CLOCK_REALTIME, &a);
                            //tsp.SetIterationsLeft(currentPopulationSize);


                            TSPresult res = tsp.RunHybridAlgorithm(currentPopulationSize, currentTimeForAlgorithm,
                                                                   currentCrossoverProbability,
                                                                   currentMutationProbability);

                            clock_gettime(CLOCK_REALTIME, &b);

                            totalTime += Time::TimeDiff(a, b);
                            totalError += (res.pathWeight - bestKnownPathWeight);
                            totalGenerationsAmount += res.generationAmount;

                        }

                        file << currentPopulationSize << ';' << currentTimeForAlgorithm << ';'
                             << currentCrossoverProbability << ';' << currentMutationProbability << ';'
                             << totalGenerationsAmount / numberOfMeasurements << ';'
                             << totalTime / numberOfMeasurements << ';'
                             << totalError / numberOfMeasurements << ';'
                             << (totalError / numberOfMeasurements) / (double) bestKnownPathWeight << ';'
                             << std::endl;


                        if (file.fail()) {
                            std::cout << "Blad zapisu wyniku pomiaru do pliku, przerywam pomiary."
                                      << std::endl;
                            return -1;
                        }


                    }
                }
            }

            std::cout << "Dokonano pomiarow dla populacji o wielkosci:  " << currentPopulationSize  << std::endl;
        }

        file.close();
        std::cout << "Zakonczono automatyczne pomiary." << std::endl;

    } else {
        std::cout << "Nie udalo sie otworzyc pliku o podanej nazwie" << std::endl;
        return -2;
    }


}