#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<algorithm>
#include<limits>
#include<queue>
#include<map>
#include<random>
#include<functional>
#include <cmath>
#include "Subset.hpp"
#include "Time.hpp"

#ifndef Tsp_h
#define Tsp_h

struct TSPnode {

    uint16_t City;
    std::vector<uint16_t> CitiesLeft;
    int64_t Weight;
    int64_t Bound;
    std::vector<uint16_t> Path;

    bool operator<(const TSPnode &other) const {

        return Bound > other.Bound;
    }

    TSPnode(uint16_t city, std::vector<uint16_t> citiesLeft) : City(city), CitiesLeft(citiesLeft) {  ; }
    TSPnode(uint16_t city, std::vector<uint16_t> citiesLeft, std::vector<uint16_t> path) : City(city), CitiesLeft(citiesLeft), Path(path) {  ; }


};


struct CityWithLeftCities {

    uint16_t City;
    std::vector<uint16_t> CitiesLeft;

    bool operator<(const CityWithLeftCities &other) const { 
        
        if(City < other.City) return true;
        if(City > other.City) return false;

        if(CitiesLeft < other.CitiesLeft) return true;
        if(CitiesLeft > other.CitiesLeft) return false;

        return false;
    }

    CityWithLeftCities(uint16_t city, std::vector<uint16_t> citiesLeft) : City(city), CitiesLeft(citiesLeft) {  ; }

};

struct WeightAndPrevious {

    int64_t Weight;
    uint16_t PreviousCity; 

    WeightAndPrevious() = default;
    WeightAndPrevious(int64_t weight, uint16_t previousCity) : Weight(weight), PreviousCity(previousCity) { ; }

};

struct TSPresult {

    std::vector<uint16_t> shortestPath;
    int64_t pathWeight;
    uint generationAmount;


    std::string toString(uint16_t startCity);


    bool operator<(const TSPresult &other) const {

        if(pathWeight < other.pathWeight) return true;
        if(pathWeight >= other.pathWeight) return false;
    }

};

struct ResultPair {

    TSPresult a;
    TSPresult b;

};

class TSP {

protected:
    std::vector<std::vector<int>> citiesMatrix;
    uint16_t citiesAmount;
    uint16_t startCity {0};


    std::vector<uint16_t> getVectorWithoutElement(const std::vector<uint16_t> &vect, uint16_t value);
    bool ifVectorContain(const std::vector<uint16_t> &vect, uint16_t value);


public:
    void ReadMatrixFromFile(std::string fileName);
    void PrintCitiesMatrix();
    void SetStartCity(uint16_t cityNumber);
    uint16_t GetStartCity() { return startCity; }
    void GenerateRandomCitiesMatrix(uint16_t matrixSize);
    TSPresult GetNewResultByInsert(TSPresult &oldResult, const std::vector<uint16_t> &pair);
    TSPresult GetNewResultBySwap(TSPresult &oldResult, const std::vector<uint16_t> &pair);

};



#endif