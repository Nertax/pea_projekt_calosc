#include "Subset.hpp"



int Subset::_length = 0;
std::vector<std::vector<uint16_t>> Subset::_vectorOfSubsets;

void Subset::setLength(int length) {

    _length = length;

}

void Subset::output(int string[], int position)
{

    bool * temp_string = new bool[_length];
    int index = 0;
    uint16_t i;
    for (i = 0; i < _length; i++)
    {
        if ((index < position) && (string[index] == i))
        {
            temp_string[i] = 1;
            index++;
        }
        else
            temp_string[i] = 0;
    }


    std::vector<uint16_t> temp;

    for (i = 0; i < _length; i++)
        if(temp_string[i] == true)
            temp.push_back(i);


    delete [] temp_string;

    _vectorOfSubsets.push_back(temp);

}
// Recursively generate the banker’s sequence.
void Subset::generate(int string[], int position, int positions)
{

    if (position < positions)
    {
        if (position == 0)
        {
            for (int i = 0; i < Subset::_length; i++)
            {
                string[position] = i;
                generate(string, position + 1, positions);
            }
        }
        else
        {
            for (int i = string[position - 1] + 1; i < Subset::_length; i++)
            {
                string[position] = i;
                generate(string, position + 1, positions);
            }
        }
    }
    else
        output(string, positions);
}

std::vector<std::vector<uint16_t>> Subset::getVectorOfSubsets(int subsetSize) {

    int * string = new int[_length];
    generate(string, 0, subsetSize);
    delete [] string;
    return _vectorOfSubsets;
}

