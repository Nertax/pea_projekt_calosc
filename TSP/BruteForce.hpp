#include "Tsp.hpp"


#ifndef PROJEKT2_BRUTEFORCE_HPP
#define PROJEKT2_BRUTEFORCE_HPP

class BruteForce : public TSP {

public:
    TSPresult RunBruteForce();

};

#endif //PROJEKT2_BRUTEFORCE_HPP
