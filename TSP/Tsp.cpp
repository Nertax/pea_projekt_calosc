#include"Tsp.hpp"


void TSP::GenerateRandomCitiesMatrix(uint16_t matrixSize) {

    std::mt19937 randomEngine(time(0));
    std::uniform_int_distribution<int> distribution(1, std::numeric_limits<int>::max());
    auto generator = std::bind(distribution, randomEngine);


    citiesAmount = matrixSize;

    citiesMatrix.resize(matrixSize);
                       
    for(size_t i = 0; i < matrixSize; ++i)
        citiesMatrix[i].resize(matrixSize);

    for(size_t i = 0; i < matrixSize; ++i)
        for(size_t j = 0; j < matrixSize; ++j) 
        {
            if(i == j)
                citiesMatrix[i][j] = 0;
            else
                citiesMatrix[i][j] = generator();
        }

}


void TSP::PrintCitiesMatrix() {

    if(citiesMatrix.empty())
        std::cout << "City Matrix is empty!" << std::endl;
    else 
    {
        for(std::vector<int> row : citiesMatrix) 
        {
            for(int x : row)
                std::cout << x << " ";

            std::cout << std::endl;
        }
    }

}

void TSP::ReadMatrixFromFile(std::string fileName) {


    std::ifstream file;
    file.open(fileName.c_str());

    if(file.is_open()) {
    
        int temp;
        file >> temp;
        citiesAmount = temp;
        if(file.fail()) 
            std::cout << "Error - can't read matrix size" << std::endl; 
        else {
        
            citiesMatrix.resize(citiesAmount);
                       
            for(size_t i = 0; i < citiesAmount; ++i)
                citiesMatrix[i].resize(citiesAmount);

            for(size_t i = 0; i < citiesAmount; ++i)
                for(size_t j = 0; j < citiesAmount; ++j) 
                {

                    file >> citiesMatrix[i][j];

                    if(file.fail())
                    {
                        std::cout << "Error during reading matrix content" << std::endl; 
                        file.close(); 
                        return; 
                    } 
                }
        }

            std::cout << "Reading complete!" << std::endl; 
            file.close(); 
    }
    else 
        std::cout << "Error - can't open file with name: " << fileName << std::endl; 

    
}
/*
void TSP::SetStartCity(uint16_t cityNumber) {

    if(cityNumber >= citiesAmount)
        std::cout << "Error - start city number greater than amount of cities" << std::endl;
    else
        startCity = cityNumber;

}*/


std::string TSPresult::toString(uint16_t  startCity) {


    std::string s = "Path weight: " + std::to_string(this->pathWeight) + '\n';
    s = s + "Path: " + std::to_string(startCity) + " - ";

    for(uint city : this->shortestPath)
        if(city != startCity) //bnb adds start city to path so we need to check it to avoid double printing startCity
            s = s + std::to_string(city) + " - ";

    s += std::to_string(startCity);

    return s;


}


bool TSP::ifVectorContain(const std::vector<uint16_t> &vect, uint16_t value) {

    for(size_t i = 0; i < vect.size(); i++) 
        if(vect[i] == value)
            return true;

    
    return false;
}


std::vector<uint16_t> TSP::getVectorWithoutElement(const std::vector<uint16_t> &vect, uint16_t value) {

    std::vector<uint16_t> newVect(vect);
    for(size_t i = 0; i < newVect.size(); i++) {
        if(newVect[i] == value) {
            newVect.erase(newVect.begin() + i);
            return newVect;
        }
    }    
}


TSPresult TSP::GetNewResultByInsert(TSPresult &oldResult, const std::vector<uint16_t> &pair) {


    TSPresult newResult = oldResult;

    std::vector<uint16_t>::iterator it = newResult.shortestPath.begin();

    uint16_t tempCity = newResult.shortestPath[pair[1]];
    newResult.shortestPath.erase(it + pair[1]);
    newResult.shortestPath.insert(it + pair[0], tempCity);

    int64_t newPathWeight = 0;

    newPathWeight += citiesMatrix[startCity][newResult.shortestPath[0]];

    for (int i = 0; i < citiesAmount - 2; i++) {
        newPathWeight += citiesMatrix[newResult.shortestPath[i]][newResult.shortestPath[i + 1]];
    }


    newPathWeight += citiesMatrix[newResult.shortestPath.back()][startCity];

    newResult.pathWeight = newPathWeight;

    return newResult;
}

TSPresult TSP::GetNewResultBySwap(TSPresult &oldResult, const std::vector<uint16_t> &pair) {


    TSPresult newResult = oldResult;



    uint16_t tempCity = newResult.shortestPath[pair[0]];
    newResult.shortestPath[pair[0]] = newResult.shortestPath[pair[1]];
    newResult.shortestPath[pair[1]] = tempCity;

    int64_t newPathWeight = 0;

    newPathWeight += citiesMatrix[startCity][newResult.shortestPath[0]];

    for (int i = 0; i < citiesAmount - 2; i++)
        newPathWeight += citiesMatrix[newResult.shortestPath[i]][newResult.shortestPath[i + 1]];

    newPathWeight += citiesMatrix[newResult.shortestPath.back()][startCity];

    newResult.pathWeight = newPathWeight;

    return newResult;
}
