#include<vector>
#include <cstdint>

#ifndef PROJEKT2_SUBSET_HPP
#define PROJEKT2_SUBSET_HPP


class Subset {

private:
    static int _length;
    static std::vector<std::vector<uint16_t>> _vectorOfSubsets;

public:
    static void output(int string[], int position);
    static void generate(int string[], int position, int positions);
    static void setLength(int length);
    static std::vector<std::vector<uint16_t>> getVectorOfSubsets(int subsetSize);
    static void resetVectorOfSubsets() { _vectorOfSubsets.clear(); }



};


#endif //PROJEKT2_SUBSET_HPP
