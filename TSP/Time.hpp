#ifndef PROJEKT2_TIME_HPP
#define PROJEKT2_TIME_HPP


#include <time.h>

class Time {

public:
    long static TimeDiff(timespec start, timespec end);

};


#endif //PROJEKT2_TIME_HPP
