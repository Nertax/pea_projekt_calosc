#include"BnB.hpp"

TSPresult BnB::BnBDFS(int64_t (BnB::*getBound)(TSPnode)) {


    std::priority_queue<TSPnode> leastBoundQueue;


    TSPresult res;
    res.pathWeight = std::numeric_limits<int64_t>::max();


    //make first node and add it to priority queue
    std::vector<uint16_t> citiesLeftFromStartCity;

    for(uint16_t i = 1; i < citiesAmount; i++) {
        citiesLeftFromStartCity.push_back(i);

    }


    TSPnode firstNode(startCity, citiesLeftFromStartCity);
    firstNode.Weight = 0;

    firstNode.Bound = (this->*getBound)(firstNode);

    leastBoundQueue.push(firstNode);

    while(!leastBoundQueue.empty()) {


        if(leastBoundQueue.top().Bound > res.pathWeight)
            break;
        else
        {
            if(leastBoundQueue.top().CitiesLeft.size() == 1)
            {
                int64_t tempWeight = leastBoundQueue.top().Weight + citiesMatrix[leastBoundQueue.top().City][leastBoundQueue.top().CitiesLeft[0]] + citiesMatrix[leastBoundQueue.top().CitiesLeft[0]][startCity];

                if(tempWeight < res.pathWeight)
                {
                    res.pathWeight = tempWeight;
                    res.shortestPath = leastBoundQueue.top().Path;
                    res.shortestPath.push_back(leastBoundQueue.top().City);
                    res.shortestPath.push_back(leastBoundQueue.top().CitiesLeft.front());

                }

                leastBoundQueue.pop();
            }
            else
            {

                TSPnode currentlyLeastBoundNode(leastBoundQueue.top());
                leastBoundQueue.pop();

                for(size_t i = 0; i < currentlyLeastBoundNode.CitiesLeft.size(); i++)
                {

                    TSPnode node(currentlyLeastBoundNode.CitiesLeft[i], getVectorWithoutElement(currentlyLeastBoundNode.CitiesLeft, currentlyLeastBoundNode.CitiesLeft[i]), currentlyLeastBoundNode.Path);
                    node.Path.push_back(currentlyLeastBoundNode.City);
                    node.Weight = currentlyLeastBoundNode.Weight + citiesMatrix[currentlyLeastBoundNode.City][currentlyLeastBoundNode.CitiesLeft[i]];

                    node.Bound = (this->*getBound)(node);

                    if(node.Bound < res.pathWeight)
                    {
                        leastBoundQueue.push(node);

                        // if(leastBoundQueue.size() > queueElemAmount)
                        //    queueElemAmount = leastBoundQueue.size();
                    }

                }
            }
        }
    }

    //std::cout << "Max queue elem amount: " << queueElemAmount << std::endl;

    return res;
}


int64_t BnB::GetBoundByMinValueInEachRow(TSPnode node)
{

    int64_t bound = node.Weight;
    int temp = std::numeric_limits<int>::max();

    for(uint16_t i = 0; i < citiesMatrix.size(); i++)
        if(citiesMatrix[node.City][i] < temp)
            if(!ifVectorContain(node.Path, i))
                temp = citiesMatrix[node.City][i];

    bound += temp;


    for(size_t i = 0; i < node.CitiesLeft.size(); i++)
    {

        temp = std::numeric_limits<int>::max();

        for(uint16_t j = 0; j < citiesMatrix.size(); j++)
            if(citiesMatrix[node.CitiesLeft[i]][j] < temp && node.CitiesLeft[i] != j)
                if(!ifVectorContain(node.Path, j) || j == 0)
                    temp = citiesMatrix[node.CitiesLeft[i]][j];


        bound += temp;
    }

    return bound;
}

int64_t BnB::GetBoundByAvgOfMinValueInRowAndColumn(TSPnode node)
{
    int64_t bound = node.Weight;
    double minInRow = std::numeric_limits<double>::max();
    double minInColumn = std::numeric_limits<double>::max();

    //exit from City
    for(uint16_t i = 0; i < node.CitiesLeft.size(); i++)
        if(citiesMatrix[node.City][node.CitiesLeft[i]] < minInRow)
            minInRow = citiesMatrix[node.City][node.CitiesLeft[i]];

    //entrance back to startCity 
    for(uint16_t i = 0; i < node.CitiesLeft.size(); i++)
        if(citiesMatrix[node.CitiesLeft[i]][startCity] < minInColumn)
            minInColumn = citiesMatrix[node.CitiesLeft[i]][startCity];

    bound += (int64_t)((minInRow + minInColumn) / 2);



    for(size_t i = 0; i < node.CitiesLeft.size(); i++)
    {

        minInRow = citiesMatrix[node.CitiesLeft[i]][startCity];;
        minInColumn = citiesMatrix[node.City][node.CitiesLeft[i]];


        //entrance to node
        for(uint16_t j = 0; j < node.CitiesLeft.size(); j++) {
            if(citiesMatrix[node.CitiesLeft[j]][node.CitiesLeft[i]] < minInColumn && i != j) {
                minInColumn = citiesMatrix[node.CitiesLeft[j]][node.CitiesLeft[i]];
            }
        }

        //exit from node
        for(uint16_t j = 0; j <  node.CitiesLeft.size(); j++) {
            if(citiesMatrix[node.CitiesLeft[i]][node.CitiesLeft[j]] < minInRow && i != j) {
                minInRow = citiesMatrix[node.CitiesLeft[i]][node.CitiesLeft[j]];
            }
        }

        bound += (int64_t)((minInRow + minInColumn) / 2);

    }

    return bound;
}

int64_t BnB::GetMaxBound(TSPnode node) {

    int64_t boundA = this->GetBoundByAvgOfMinValueInRowAndColumn(node);
    int64_t boundB = this->GetBoundByMinValueInEachRow(node);

    if(boundA > boundB)
        return boundA;
    else
        return boundB;

}