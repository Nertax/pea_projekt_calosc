#include "Tsp.hpp"

#ifndef PROJEKT2_BNB_HPP
#define PROJEKT2_BNB_HPP

class BnB : public TSP {

public:
    int64_t GetBoundByMinValueInEachRow(TSPnode node);
    int64_t GetBoundByAvgOfMinValueInRowAndColumn(TSPnode node);
    int64_t GetMaxBound(TSPnode node);
    TSPresult BnBDFS(int64_t (BnB::*getBound)(TSPnode));


};

#endif //PROJEKT2_BNB_HPP
