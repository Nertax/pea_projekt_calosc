#include"GeneticAlgorithm.hpp"


TSPresult GeneticAlgorithm::RunGeneticAlgorithm(uint populationSize, long timeForAlgorithm, double crossoverProbability,
                                                double mutationProbability) {

    uint generationAmount = 0;
    struct timespec a, b;
    clock_gettime(CLOCK_REALTIME, &a);

    std::mt19937 generator (std::random_device{}());
    std::uniform_int_distribution<uint16_t> distributionOperatorCrossover(1, citiesAmount - 3); // -3 => -1 (we count from 0), -1 (we dont want start city here), -1 (we dont want to copy whole vector, last elem should be left)
    std::uniform_int_distribution<uint16_t> distributionSwapMutation(0, citiesAmount - 2); // -3 => -1 (we count from 0), -1 (we dont want start city here)


    std::default_random_engine engineForDecision;
    std::uniform_real_distribution<double> distributionForDecision(0.0, 1.0);
    auto getRandomNumberToMakeDecision = std::bind(distributionForDecision, generator);

    std::vector<TSPresult> population(populationSize);

    for(TSPresult& current : population)
    {

        current.shortestPath.resize(citiesAmount - 1); //without start city
        std::iota(current.shortestPath.begin(), current.shortestPath.end(), 1);
        std::shuffle(current.shortestPath.begin(), current.shortestPath.end(), generator);

        current.pathWeight = citiesMatrix[startCity][current.shortestPath[0]];

        for(size_t i = 0; i < current.shortestPath.size() - 1; i++)
            current.pathWeight += citiesMatrix[current.shortestPath[i]][current.shortestPath[i+1]];

        current.pathWeight += citiesMatrix[current.shortestPath[current.shortestPath.size() - 1]][startCity];

        current.generationAmount = 0;

    }

    TSPresult result;

    clock_gettime(CLOCK_REALTIME, &b);

    while(Time::TimeDiff(a, b) < timeForAlgorithm)
    {

        std::vector<uint> parentsInRandomPairs(populationSize);
        std::iota(parentsInRandomPairs.begin(), parentsInRandomPairs.end(), 0);
        std::shuffle(parentsInRandomPairs.begin(), parentsInRandomPairs.end(), generator);

        for(size_t i = 0; i < populationSize / 2; i += 2)
        {

            population[parentsInRandomPairs[i]].generationAmount++;
            population[parentsInRandomPairs[i + 1]].generationAmount++;

            double temp = getRandomNumberToMakeDecision();

            if(crossoverProbability > temp)
            {
                ResultPair parents;
                parents.a = population[parentsInRandomPairs[i]];
                parents.b = population[parentsInRandomPairs[i + 1]];


                uint16_t k1 = distributionOperatorCrossover(generator); //k1 - left
                uint16_t k2 = distributionOperatorCrossover(generator); //k2 - right


                while (k1 == k2)
                    k2 = distributionOperatorCrossover(generator);

                ResultPair children = _OXoperator(parents, k1, k2);

                if(mutationProbability > getRandomNumberToMakeDecision()) {

                    uint16_t swap1 = distributionSwapMutation(generator);
                    uint16_t swap2 = distributionSwapMutation(generator);


                    while (swap1 == swap2)
                        swap2 = distributionSwapMutation(generator);

                    _SwapMutation(children, swap1, swap2);
                }

                population.push_back(children.a);
                population.push_back(children.b);


            }

        }


        std::stable_sort(population.begin(), population.end());

        population.resize(populationSize);

        for(size_t i = 1; i < population.size(); i++) {
            if (population[i].generationAmount > 10) {
                std::shuffle(population[i].shortestPath.begin(), population[i].shortestPath.end(), generator);


                population[i].pathWeight = citiesMatrix[startCity][population[i].shortestPath[0]];

                for (size_t j = 0; j < population[i].shortestPath.size() - 1; j++)
                    population[i].pathWeight += citiesMatrix[population[i].shortestPath[j]][population[i].shortestPath[
                            j + 1]];

                population[i].pathWeight += citiesMatrix[population[i].shortestPath[population[i].shortestPath.size() -
                                                                                    1]][startCity];

                population[i].generationAmount = 0;
            }
        }


        generationAmount++;
        clock_gettime(CLOCK_REALTIME, &b);
    }


    std::sort(population.begin(), population.end());


    //clock_gettime(CLOCK_REALTIME, &b);
   // std::cout << "Ilosc pokolen: " << generationAmount << " rzeczywisty czas: " << Time::TimeDiff(a, b) << std::endl;

    population[0].generationAmount = generationAmount;

    return population[0];
}

TSPresult GeneticAlgorithm::RunHybridAlgorithm(uint populationSize, long timeForAlgorithm, double crossoverProbability,
                                               double mutationProbability) {

    uint generationAmount = 0;
    struct timespec a, b;
    clock_gettime(CLOCK_REALTIME, &a);

    std::mt19937 generator (std::random_device{}());
    std::uniform_int_distribution<uint16_t> distributionOperatorCrossover(1, citiesAmount - 3); // -3 => -1 (we count from 0), -1 (we dont want start city here), -1 (we dont want to copy whole vector, last elem should be left)
    std::uniform_int_distribution<uint16_t> distributionSwapMutation(0, citiesAmount - 2); // -3 => -1 (we count from 0), -1 (we dont want start city here)


    std::default_random_engine engineForDecision;
    std::uniform_real_distribution<double> distributionForDecision(0.0, 1.0);
    auto getRandomNumberToMakeDecision = std::bind(distributionForDecision, generator);

    std::vector<TSPresult> population(populationSize);

    for(TSPresult& current : population)
    {

        current.shortestPath.resize(citiesAmount - 1); //without start city
        std::iota(current.shortestPath.begin(), current.shortestPath.end(), 1);
        std::shuffle(current.shortestPath.begin(), current.shortestPath.end(), generator);

        current.pathWeight = citiesMatrix[startCity][current.shortestPath[0]];

        for(size_t i = 0; i < current.shortestPath.size() - 1; i++)
            current.pathWeight += citiesMatrix[current.shortestPath[i]][current.shortestPath[i+1]];

        current.pathWeight += citiesMatrix[current.shortestPath[current.shortestPath.size() - 1]][startCity];

        current.generationAmount = 0;

    }

    TSPresult result;

    clock_gettime(CLOCK_REALTIME, &b);

    while(Time::TimeDiff(a, b) < timeForAlgorithm)
    {

        std::vector<uint> parentsInRandomPairs(populationSize);
        std::iota(parentsInRandomPairs.begin(), parentsInRandomPairs.end(), 0);
        std::shuffle(parentsInRandomPairs.begin(), parentsInRandomPairs.end(), generator);

        for(size_t i = 0; i < populationSize / 2; i += 2)
        {

            population[parentsInRandomPairs[i]].generationAmount++;
            population[parentsInRandomPairs[i + 1]].generationAmount++;

            double temp = getRandomNumberToMakeDecision();

            if(crossoverProbability > temp)
            {
                ResultPair parents;
                parents.a = population[parentsInRandomPairs[i]];
                parents.b = population[parentsInRandomPairs[i + 1]];


                uint16_t k1 = distributionOperatorCrossover(generator); //k1 - left
                uint16_t k2 = distributionOperatorCrossover(generator); //k2 - right


                while (k1 == k2)
                    k2 = distributionOperatorCrossover(generator);

                ResultPair children = _OXoperator(parents, k1, k2);

                if(mutationProbability > getRandomNumberToMakeDecision()) {

                    uint16_t swap1 = distributionSwapMutation(generator);
                    uint16_t swap2 = distributionSwapMutation(generator);


                    while (swap1 == swap2)
                        swap2 = distributionSwapMutation(generator);

                    _SwapMutation(children, swap1, swap2);
                }

                population.push_back(children.a);
                population.push_back(children.b);


            }

        }


        std::stable_sort(population.begin(), population.end());

        population.resize(populationSize);

        for(size_t i = 1; i < population.size(); i++) {
            if (population[i].generationAmount > 10) {
                std::shuffle(population[i].shortestPath.begin(), population[i].shortestPath.end(), generator);


                population[i].pathWeight = citiesMatrix[startCity][population[i].shortestPath[0]];

                for (size_t j = 0; j < population[i].shortestPath.size() - 1; j++)
                    population[i].pathWeight += citiesMatrix[population[i].shortestPath[j]][population[i].shortestPath[
                            j + 1]];

                population[i].pathWeight += citiesMatrix[population[i].shortestPath[population[i].shortestPath.size() -
                                                                                    1]][startCity];

                population[i].generationAmount = 0;
            }
        }


        generationAmount++;
        clock_gettime(CLOCK_REALTIME, &b);
    }

    for(auto& current : population)
    {
        _FindLocalMinForTSPresult(current, &SimulatedAnnealing::GetNewResultByInsert);

    }

    std::sort(population.begin(), population.end());


  //  clock_gettime(CLOCK_REALTIME, &b);
  //  std::cout << "Ilosc pokolen: " << generationAmount << " rzeczywisty czas: " << Time::TimeDiff(a, b) << std::endl;

    population[0].generationAmount = generationAmount;

    return population[0];
}


void GeneticAlgorithm::_SwapMutation(ResultPair &children, uint16_t firstPosition, uint16_t secondPosition)
{

    if(firstPosition >= children.a.shortestPath.size() || secondPosition >= children.a.shortestPath.size())
        throw std::out_of_range("Position greater than vector size during swap mutation");

    std::swap(children.a.shortestPath[firstPosition], children.a.shortestPath[secondPosition]);
    std::swap(children.b.shortestPath[firstPosition], children.b.shortestPath[secondPosition]);

}


ResultPair GeneticAlgorithm::_OXoperator(const ResultPair &parents, uint16_t k1, uint16_t k2) { //k1 - left, k2 - right


    if (k1 > k2)
        std::swap(k1, k2);


    std::vector<uint16_t > childA(citiesAmount - 1);
    std::vector<uint16_t > childB(citiesAmount - 1);


    for (size_t i = k1; i <= k2; i++) {
        childA[i] = parents.a.shortestPath[i];
        childB[i] = parents.b.shortestPath[i];

    }


    uint16_t posA = k2 + 1;
    uint16_t posB = k2 + 1;

    for (size_t i = k2 + 1; i < citiesAmount - 1; i++) {


        while(ifVectorContain(childA, parents.b.shortestPath[posA]))
            posA = (posA + 1) % (citiesAmount - 1);

        while(ifVectorContain(childB, parents.a.shortestPath[posB]))
            posB = (posB + 1) % (citiesAmount - 1);

        childA[i] = parents.b.shortestPath[posA];
        childB[i] = parents.a.shortestPath[posB];

    }

    for(size_t i = 0; i < k1; i++)
    {
        while(ifVectorContain(childA, parents.b.shortestPath[posA]))
            posA = (posA + 1) % (citiesAmount - 1);

        while(ifVectorContain(childB, parents.a.shortestPath[posB]))
            posB = (posB + 1) % (citiesAmount - 1);

        childA[i] = parents.b.shortestPath[posA];
        childB[i] = parents.a.shortestPath[posB];
    }


    int64_t childAPathWeight = 0;
    int64_t childBPathWeight = 0;

    childAPathWeight += citiesMatrix[startCity][childA[0]];
    childBPathWeight += citiesMatrix[startCity][childB[0]];

    for (size_t i = 0; i < citiesAmount - 2; i++) {
        childAPathWeight += citiesMatrix[childA[i]][childA[i + 1]];
        childBPathWeight += citiesMatrix[childB[i]][childB[i + 1]];
    }


    childAPathWeight += citiesMatrix[childA.back()][startCity];
    childBPathWeight += citiesMatrix[childB.back()][startCity];

    ResultPair children;

    children.a.shortestPath = childA;
    children.a.pathWeight = childAPathWeight;

    children.b.shortestPath = childB;
    children.b.pathWeight = childBPathWeight;

    return  children;
}




void GeneticAlgorithm::_FindLocalMinForTSPresult(TSPresult &result, TSPresult (TSP::*getNewResult)(TSPresult &oldResult,
                                                                                                   const std::vector<uint16_t> &pair))
{

    Subset::setLength(citiesAmount - 1);
    Subset::resetVectorOfSubsets();
    std::vector<std::vector<uint16_t>> setOfPairsForGenerateNeighbours = Subset::getVectorOfSubsets(2);

    std::mt19937 randomEngine(std::random_device{}());
    std::uniform_int_distribution<int> distribution(0, setOfPairsForGenerateNeighbours.size() - 1);
    auto generator = std::bind(distribution, randomEngine);


    bool noBetterResultsFound = false;
    while (!noBetterResultsFound) {

        int leftNeighboursAmount = setOfPairsForGenerateNeighbours.size();
        std::vector<bool> usedNeighbourFlags(setOfPairsForGenerateNeighbours.size());

        while (leftNeighboursAmount) {

            int numberOfPairToUse;

            while (true) {
                numberOfPairToUse = generator();
                if (usedNeighbourFlags[numberOfPairToUse] == false) {
                    usedNeighbourFlags[numberOfPairToUse] = true;
                    break;
                } else
                    continue;
            }

            TSPresult tempResult = (this->*getNewResult)(result, setOfPairsForGenerateNeighbours[numberOfPairToUse]);

            if (tempResult.pathWeight < result.pathWeight) {
                result.pathWeight = tempResult.pathWeight;
                result.shortestPath = tempResult.shortestPath;

                break; // greedy

            }  else {
                leftNeighboursAmount--;

                usedNeighbourFlags[numberOfPairToUse] = false;

                if (leftNeighboursAmount == 0)
                    noBetterResultsFound = true;

            }
        }
    }
}

