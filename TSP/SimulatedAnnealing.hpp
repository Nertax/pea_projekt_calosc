#include "Tsp.hpp"


#ifndef PROJEKT2_SIMULATEDANNEALING_HPP
#define PROJEKT2_SIMULATEDANNEALING_HPP

class SimulatedAnnealing : public TSP {

public:
    TSPresult RunSimulatedAnnealing(double temperature, double coolingRate,  double aboveTemperatureChooseNeighbour, double temperatureUp, uint iterationsAmountToRestart, TSPresult (TSP::*getNewResult)(TSPresult &oldResult, const std::vector<uint16_t> &pair));
    uint GetLocalMinAmount() const;
    void SetIterationsLeft(uint iterationsLeft);


private:
    uint _localMinAmount {0};
    uint _iterationsLeft {0};

    TSPresult _GetRandomResult();
    static double _GetCurrentProbability(int64_t currentPathWeight, int64_t newPathWeight, double currentTemperature);
    static double _GetNewTemperatureL(double currentTemperature, double coolingRate);

};

#endif //PROJEKT2_SIMULATEDANNEALING_HPP
