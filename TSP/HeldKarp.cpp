#include "HeldKarp.hpp"



TSPresult HeldKarp::RunHeldKarp() {


    TSPresult result;
    std::map<CityWithLeftCities, WeightAndPrevious> mapOfReadyNodes;


    //from 1 becasue 0 is our start city
    for(uint16_t i = 1; i < citiesAmount; i++) {

        std::vector<uint16_t> temp;
        mapOfReadyNodes[CityWithLeftCities(i, temp)] = WeightAndPrevious(citiesMatrix[i][startCity], startCity);
    }

    Subset::setLength(citiesAmount); // set length - set size of the set from we will make subsets

    for(int subsetLength = 1; subsetLength < citiesAmount - 1; subsetLength++) {
        Subset::resetVectorOfSubsets();
        std::vector<std::vector<uint16_t>> temp = Subset::getVectorOfSubsets(subsetLength); // make vector of all subsets of size subsetLength

        for(size_t m = 0; m < temp.size(); m++) { //g(?,|{3}|{4}|) = c23 + g(3, ∅ ) = c23 + c31 = 6 + 15 = 21

            if(!ifVectorContain(temp[m], startCity)) {
                for(uint16_t j = 0; j < citiesAmount; j++) { //g(|2|4|,{3}) = c23 + g(3, ∅ ) = c23 + c31 = 6 + 15 = 21

                    if(!ifVectorContain(temp[m], j) && j != startCity) {

                        int64_t tempPath;
                        mapOfReadyNodes[CityWithLeftCities(j, temp[m])] = WeightAndPrevious(std::numeric_limits<int64_t>::max(), 0);

                        for(size_t k = 0; k < temp[m].size(); k++) {

                            tempPath = citiesMatrix[j][temp[m][k]] + mapOfReadyNodes[CityWithLeftCities(temp[m][k], getVectorWithoutElement(temp[m], temp[m][k]))].Weight;

                            if(tempPath < mapOfReadyNodes[CityWithLeftCities(j, temp[m])].Weight) {
                                mapOfReadyNodes[CityWithLeftCities(j, temp[m])].Weight = tempPath;
                                mapOfReadyNodes[CityWithLeftCities(j, temp[m])].PreviousCity = temp[m][k];
                            }
                        }
                    }
                }
            }
        }
    }

    //std::cout << "Map size: " << mapOfReadyNodes.size() << std::endl;


    int64_t res = std::numeric_limits<int64_t>::max();
    uint16_t prev = 0;

    int64_t tempRes;

    std::vector<uint16_t> vectorWithoutStartCity;

    //make vector of all cities without start one
    for(uint16_t i = 0; i < citiesAmount; i++)
        if(i != startCity)
            vectorWithoutStartCity.push_back(i);

    //last step of Held Karp
    for(size_t i = 0; i < vectorWithoutStartCity.size(); i++) {

        tempRes = citiesMatrix[startCity][vectorWithoutStartCity[i]] + mapOfReadyNodes[CityWithLeftCities(vectorWithoutStartCity[i], getVectorWithoutElement(vectorWithoutStartCity, vectorWithoutStartCity[i]))].Weight;

        if(tempRes < res) {
            res = tempRes;
            prev = vectorWithoutStartCity[i];
        }
    }


    result.pathWeight = res;

    while(!vectorWithoutStartCity.empty()) {

        result.shortestPath.push_back(prev);
        vectorWithoutStartCity = getVectorWithoutElement(vectorWithoutStartCity, prev);
        prev = mapOfReadyNodes[CityWithLeftCities(prev, vectorWithoutStartCity)].PreviousCity;

    }

    return result;
}