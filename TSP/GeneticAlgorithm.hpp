#include "Tsp.hpp"
#include "SimulatedAnnealing.hpp"

#ifndef PROJEKT2_GENETICALGORITHM_HPP
#define PROJEKT2_GENETICALGORITHM_HPP

class GeneticAlgorithm : public TSP {

public:
    TSPresult RunHybridAlgorithm(uint populationSize, long timeForAlgorithm, double crossoverProbability,
                                 double mutationProbability);
    TSPresult RunGeneticAlgorithm(uint populationSize, long timeForAlgorithm, double crossoverProbability,
                                  double mutationProbability);

private:
    ResultPair _OXoperator(const ResultPair &parents, uint16_t k1, uint16_t k2);
    void _SwapMutation(ResultPair &children, uint16_t firstPosition, uint16_t secondPosition);
    void _FindLocalMinForTSPresult(TSPresult &result, TSPresult (TSP::*getNewResult)(TSPresult &oldResult,
                                                                                     const std::vector<uint16_t> &pair));


};

#endif //PROJEKT2_GENETICALGORITHM_HPP
