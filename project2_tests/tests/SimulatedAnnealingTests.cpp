
#include <gtest/gtest.h>
#include <TSP/Tsp.hpp>


struct SimulatedAnnealingNewResultTest : public testing::Test {

    TSPresult startingResult;
    TSP tsp;


    void SetUp() {
        startingResult.shortestPath = std::vector<uint16_t>{1, 2, 3, 4, 5};
        tsp.ReadMatrixFromFile("../test_data/tsp_6_1.txt");
    }

    void TearDown() {}
};


TEST_F(SimulatedAnnealingNewResultTest, InsertOneToZero) {
    const std::vector<uint16_t> pair{0, 1};
    std::vector<uint16_t> expectedResult{2, 1, 3, 4, 5};

    TSPresult newResult = tsp.GetNewResultByInsert(startingResult, pair);

    for (int i = 0; i < expectedResult.size(); i++) {
        EXPECT_EQ(newResult.shortestPath[i], expectedResult[i]);
    }

    EXPECT_EQ(newResult.pathWeight, 156);
}

TEST_F(SimulatedAnnealingNewResultTest, InsertLastToTwo) {
    const std::vector<uint16_t> pair{2, 4};
    std::vector<uint16_t> expectedResult{1, 2, 5, 3, 4};

    TSPresult newResult = tsp.GetNewResultByInsert(startingResult, pair);

    for (int i = 0; i < expectedResult.size(); i++) {
        EXPECT_EQ(newResult.shortestPath[i], expectedResult[i]);

    }

    EXPECT_EQ(newResult.pathWeight, 184);
}

TEST_F(SimulatedAnnealingNewResultTest, InsertZeroToZero) {
    const std::vector<uint16_t> pair{0, 0};
    std::vector<uint16_t> expectedResult{1, 2, 3, 4, 5};


    TSPresult newResult = tsp.GetNewResultByInsert(startingResult, pair);

    for (int i = 0; i < expectedResult.size(); i++) {
        EXPECT_EQ(newResult.shortestPath[i], expectedResult[i]);

    }

    EXPECT_EQ(newResult.pathWeight, 132);
}

TEST_F(SimulatedAnnealingNewResultTest, InsertZeroToThree) {
    const std::vector<uint16_t> pair{3, 0};
    std::vector<uint16_t> expectedResult{2, 3, 4, 1, 5};


    TSPresult newResult = tsp.GetNewResultByInsert(startingResult, pair);

    for (int i = 0; i < expectedResult.size(); i++) {
        EXPECT_EQ(newResult.shortestPath[i], expectedResult[i]);

    }

    EXPECT_EQ(newResult.pathWeight, 178);
}

TEST_F(SimulatedAnnealingNewResultTest, SwapZeroToOne) {
    const std::vector<uint16_t> pair{0, 1};
    std::vector<uint16_t> expectedResult{2, 1, 3, 4, 5};


    TSPresult newResult = tsp.GetNewResultBySwap(startingResult, pair);

    for (int i = 0; i < expectedResult.size(); i++) {
        EXPECT_EQ(newResult.shortestPath[i], expectedResult[i]);

    }

    EXPECT_EQ(newResult.pathWeight, 156);
}


TEST_F(SimulatedAnnealingNewResultTest, SwapOneToZero) {
    const std::vector<uint16_t> pair{1, 0};
    std::vector<uint16_t> expectedResult{2, 1, 3, 4, 5};


    TSPresult newResult = tsp.GetNewResultBySwap(startingResult, pair);


    for (int i = 0; i < expectedResult.size(); i++) {
        EXPECT_EQ(newResult.shortestPath[i], expectedResult[i]);

    }

    EXPECT_EQ(newResult.pathWeight, 156);
}


TEST_F(SimulatedAnnealingNewResultTest, SwapTwoToLast) {
    const std::vector<uint16_t> pair{2, 4};
    std::vector<uint16_t> expectedResult{1, 2, 5, 4, 3};


    TSPresult newResult = tsp.GetNewResultBySwap(startingResult, pair);

    for (int i = 0; i < expectedResult.size(); i++) {
        EXPECT_EQ(newResult.shortestPath[i], expectedResult[i]);

    }

    EXPECT_EQ(newResult.pathWeight, 183);
}

TEST_F(SimulatedAnnealingNewResultTest, SwapOneToOne) {
    const std::vector<uint16_t> pair{1, 1};
    std::vector<uint16_t> expectedResult{1, 2, 3, 4, 5};

    TSPresult newResult = tsp.GetNewResultBySwap(startingResult, pair);

    for (int i = 0; i < expectedResult.size(); i++) {
        EXPECT_EQ(newResult.shortestPath[i], expectedResult[i]);

    }

    EXPECT_EQ(newResult.pathWeight, 132);
}


